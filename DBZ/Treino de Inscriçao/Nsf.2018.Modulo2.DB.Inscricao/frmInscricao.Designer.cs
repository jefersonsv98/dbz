﻿namespace Nsf._2018.Modulo2.DB.Inscricao
{
    partial class FrmInscricao
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtNome = new System.Windows.Forms.TextBox();
            this.txtFaculdade = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.Inscrever = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.dgvtabela = new System.Windows.Forms.DataGridView();
            this.button2 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.Nome = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Faculdade = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvtabela)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Black;
            this.label1.ForeColor = System.Drawing.Color.Gold;
            this.label1.Location = new System.Drawing.Point(6, 332);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 28);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nome:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtNome
            // 
            this.txtNome.Location = new System.Drawing.Point(105, 331);
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(198, 29);
            this.txtNome.TabIndex = 1;
            // 
            // txtFaculdade
            // 
            this.txtFaculdade.Location = new System.Drawing.Point(105, 367);
            this.txtFaculdade.Name = "txtFaculdade";
            this.txtFaculdade.Size = new System.Drawing.Size(198, 29);
            this.txtFaculdade.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Black;
            this.label2.ForeColor = System.Drawing.Color.Gold;
            this.label2.Location = new System.Drawing.Point(6, 367);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 29);
            this.label2.TabIndex = 2;
            this.label2.Text = "Faculdade:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Inscrever
            // 
            this.Inscrever.BackColor = System.Drawing.Color.Gold;
            this.Inscrever.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Inscrever.Location = new System.Drawing.Point(6, 402);
            this.Inscrever.Name = "Inscrever";
            this.Inscrever.Size = new System.Drawing.Size(297, 39);
            this.Inscrever.TabIndex = 4;
            this.Inscrever.Text = "Inscrever";
            this.Inscrever.UseVisualStyleBackColor = false;
            this.Inscrever.Click += new System.EventHandler(this.button1_Click);
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Black;
            this.label3.ForeColor = System.Drawing.Color.Gold;
            this.label3.Location = new System.Drawing.Point(-3, 322);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(314, 131);
            this.label3.TabIndex = 5;
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dgvtabela
            // 
            this.dgvtabela.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvtabela.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Nome,
            this.Faculdade});
            this.dgvtabela.Location = new System.Drawing.Point(445, 5);
            this.dgvtabela.Name = "dgvtabela";
            this.dgvtabela.Size = new System.Drawing.Size(347, 172);
            this.dgvtabela.TabIndex = 6;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Gold;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Location = new System.Drawing.Point(445, 181);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(347, 39);
            this.button2.TabIndex = 7;
            this.button2.Text = "Ver inscritos";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Black;
            this.label4.ForeColor = System.Drawing.Color.Gold;
            this.label4.Location = new System.Drawing.Point(438, -6);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(370, 231);
            this.label4.TabIndex = 8;
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Nome
            // 
            this.Nome.HeaderText = "Nome";
            this.Nome.Name = "Nome";
            // 
            // Faculdade
            // 
            this.Faculdade.HeaderText = "Faculdade";
            this.Faculdade.Name = "Faculdade";
            // 
            // frmInscricao
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Nsf._2018.Modulo2.DB.Inscricao.Properties.Resources.simpsons;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(800, 449);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.dgvtabela);
            this.Controls.Add(this.txtFaculdade);
            this.Controls.Add(this.txtNome);
            this.Controls.Add(this.Inscrever);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "frmInscricao";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Inscrições 2019";
            ((System.ComponentModel.ISupportInitialize)(this.dgvtabela)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtNome;
        private System.Windows.Forms.TextBox txtFaculdade;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button Inscrever;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView dgvtabela;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nome;
        private System.Windows.Forms.DataGridViewTextBoxColumn Faculdade;
    }
}

