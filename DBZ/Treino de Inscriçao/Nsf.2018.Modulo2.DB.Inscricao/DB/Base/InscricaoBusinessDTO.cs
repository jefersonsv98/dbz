﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo2.DB.Inscricao.DB.Base
{
    class InscricaoBusinessDTO
    {
        public int Salvar(InscricaoDTO dto)
        {
            if (dto.Nome == string.Empty)
                throw new ArgumentException("Nome e obrigatorio");
            if (dto.Faculdade == string.Empty)
                throw new ArgumentException("Faculdade e obrigatorio");

            InscricaoDatabaseDTO db = new InscricaoDatabaseDTO();
            return db.Salvar(dto);
        }
        public List<InscricaoDTO> Listar()
        {
            InscricaoDatabaseDTO db = new InscricaoDatabaseDTO();
            return db.Listar();
        }

    }

}
    

