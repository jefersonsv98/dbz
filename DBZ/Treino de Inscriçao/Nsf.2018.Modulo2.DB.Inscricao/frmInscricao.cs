﻿using Nsf._2018.Modulo2.DB.Inscricao.DB.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf._2018.Modulo2.DB.Inscricao
{
    public partial class FrmInscricao : Form
    {
        public FrmInscricao()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {   
            InscricaoDTO dto = new InscricaoDTO();
            dto.Nome = txtNome.Text;
            dto.Faculdade = txtFaculdade.Text;

            InscricaoBusinessDTO business = new InscricaoBusinessDTO();
            business.Salvar(dto);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            InscricaoBusinessDTO business = new InscricaoBusinessDTO();
            List<InscricaoDTO> lista = business.Listar();

            dgvtabela.AutoGenerateColumns = false;
            dgvtabela.DataSource = lista;
        }
    }
}
